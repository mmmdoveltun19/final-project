package com.taskapplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taskapplication.dto.OrganizationDTO;
import com.taskapplication.dto.OrganizationRequest;
import com.taskapplication.service.OrganizationService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrganizationController.class)
@RunWith(SpringRunner.class)
public class OrganizationControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private OrganizationService organizationService;

    @Test
    void getById() throws Exception {
        Long organizationId = 1L;
        OrganizationDTO organizationDTO = new OrganizationDTO();

        when(organizationService.getOrganizationById(organizationId)).thenReturn(organizationDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/organization/{id}", organizationId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(organizationDTO.getOrganizationName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value(organizationDTO.getPhoneNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value(organizationDTO.getAddress()));
    }

    @Test
    void updateOrganization() throws Exception {
        Long organizationId = 1L;
        OrganizationRequest organizationRequest = new OrganizationRequest();

        mockMvc.perform(MockMvcRequestBuilders.put("/organization/{id}", organizationId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(organizationRequest)))
                .andExpect(status().isOk());

        verify(organizationService, times(1)).updateOrganization(eq(organizationId),
                eq(organizationRequest));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
