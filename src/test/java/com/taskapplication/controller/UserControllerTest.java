package com.taskapplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taskapplication.dto.UserCreateResponse;
import com.taskapplication.dto.UserDTO;
import com.taskapplication.dto.UserRequest;
import com.taskapplication.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserControllerTest.class)
@RunWith(SpringRunner.class)
public class UserControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private UserService userService;

    @Test
    void addUser() throws Exception {
        UserRequest userRequest = new UserRequest();
        UserCreateResponse userCreateResponse = new UserCreateResponse();

        when(userService.addUser(userRequest)).thenReturn(userCreateResponse);

        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userRequest)))
                .andExpect(status().isCreated());
//                .andExpect(MockMvcResultMatchers.jsonPath("$.userId").value(userCreateResponse.getUserId()))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(userCreateResponse.getMessage()));
    }

    @Test
    void getAllUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        List<UserDTO> userList = Arrays.asList(userDTO);

        when(userService.getAllUsers()).thenReturn(userList);

        mockMvc.perform(MockMvcRequestBuilders.get("/user")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(userDTO.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].email").value(userDTO.getEmail()));
    }

    @Test
    void getUserById() throws Exception {
        Long userId = 1L;
        UserDTO userDTO = new UserDTO();

        when(userService.getUserById(userId)).thenReturn(userDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(userDTO.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(userDTO.getEmail()));
    }

    @Test
    void updateUser() throws Exception {
        Long userId = 1L;
        UserRequest userRequest = new UserRequest();

        mockMvc.perform(MockMvcRequestBuilders.put("/user/{id}", userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userRequest)))
                .andExpect(status().isOk());

        verify(userService, times(1)).updateUserById(eq(userId), eq(userRequest));
    }

    @Test
    void deleteUser() throws Exception {
        Long userId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(userService, times(1)).deleteUserById(eq(userId));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
