package com.taskapplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taskapplication.dto.TaskDto;
import com.taskapplication.dto.TaskRequest;
import com.taskapplication.enums.TaskStatus;
import com.taskapplication.service.TaskService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static javax.management.Query.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@WebMvcTest(TaskController.class)
@RunWith(SpringRunner.class)
public class TestTaskController {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private TaskService taskService;

    @Test
    void createTask() throws Exception {
        TaskRequest taskRequest = new TaskRequest();

        mockMvc.perform(MockMvcRequestBuilders.post("/task")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(taskRequest)))
                .andExpect(status().isOk());

        verify(taskService, times(1)).createTask((TaskRequest) eq(taskRequest));
    }

    private Object eq(TaskRequest taskRequest) {
        return taskRequest;
    }

    @Test
    void updateTaskById() throws Exception {
        Long taskId = 1L;
        TaskRequest taskRequest = new TaskRequest();

        mockMvc.perform(MockMvcRequestBuilders.put("/task/{id}", taskId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(taskRequest)))
                .andExpect(status().isOk());

        verify(taskService, times(1)).updateTask(eq(taskId), eq(taskRequest));
    }

    @Test
    void getTaskById() throws Exception {
        Long taskId = 1L;
        TaskDto taskDto = new TaskDto();

        when(taskService.getTaskById(taskId)).thenReturn(taskDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/task/{id}", taskId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(taskDto.getTitle()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(taskDto.getDescription()));
    }

    @Test
    void getAllTasksByUserId() throws Exception {
        Long userId = 1L;
        TaskDto taskDto = new TaskDto();
        List<TaskDto> taskList = Arrays.asList(taskDto);

        when(taskService.getAllTasksByUserId(userId)).thenReturn(taskList);

        mockMvc.perform(MockMvcRequestBuilders.get("/task/user-tasks/{id}", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(taskDto.getTitle()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(taskDto.getDescription()));
    }

    @Test
    void getAllTaskByOrgId() throws Exception {
        Long orgId = 1L;
        TaskDto taskDto = new TaskDto();
        List<TaskDto> taskList = Arrays.asList(taskDto);

        when(taskService.getAllTasksByOrganizationId(orgId)).thenReturn(taskList);

        mockMvc.perform(MockMvcRequestBuilders.get("/task/org/{orgId}", orgId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(taskDto.getTitle()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(taskDto.getDescription()));
    }

    @Test
    void getTasksByStatusId() throws Exception {
        String status = "PENDING";
        TaskDto taskDto = new TaskDto();
        List<TaskDto> taskList = Arrays.asList(taskDto);

        when(taskService.getAllTasksByStatus(TaskStatus.PENDING)).thenReturn(taskList);

        mockMvc.perform(MockMvcRequestBuilders.get("/task/status/")
                        .param("status", status)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(taskDto.getTitle()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(taskDto.getDescription()));
    }

    @Test
    void deleteTaskById() throws Exception {
        Long taskId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/task/{id}", taskId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(taskService, times(1)).deleteTask(eq(taskId));
    }


    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
