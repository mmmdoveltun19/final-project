package com.taskapplication.service;

import com.taskapplication.dto.OrganizationDTO;
import com.taskapplication.dto.OrganizationRequest;
import com.taskapplication.exception.NotAllowedException;
import com.taskapplication.model.Organization;
import com.taskapplication.model.User;
import com.taskapplication.repository.OrganizationRepository;
import com.taskapplication.service.auth.AuthService;
import com.taskapplication.service.impl.OrganizationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrganizationServiceImplTest {

    private static final Long ORG_ID = 1L;
    @InjectMocks
    private OrganizationServiceImpl organizationService;

    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private AuthService authService;
    @Mock
    private ModelMapper modelMapper;

    @Test
    public void testGetOrganizationByIdWhenOrganizationExistsThenReturnOrganizationDTO() {
        //Arrange
        Organization organization = new Organization();
        organization.setId(ORG_ID);
        OrganizationDTO expectedOrganizationDTO = new OrganizationDTO();
        when(organizationRepository.findById(ORG_ID)).thenReturn(Optional.of(organization));
        when(modelMapper.map(organization, OrganizationDTO.class)).thenReturn(expectedOrganizationDTO);

        //Act
        OrganizationDTO result = organizationService.getOrganizationById(ORG_ID);

        //Assert
        assertNotNull(result);
        assertEquals(expectedOrganizationDTO, result);
    }

    @Test
    public void testGetOrganizationByIdWhenOrganizationDoesNotExistsThenThrowEntityNotFoundException() {
        // Arrange
        when(organizationRepository.findById(ORG_ID)).thenReturn(Optional.empty());

        // Act & Assert
        assertThatThrownBy(() -> organizationService.getOrganizationById(ORG_ID))
                .isInstanceOf(IllegalArgumentException.class);

    }

    @Test
    public void testUpdateOrganizationWhenAllowedThenUpdateOrganization() {
        //Arrange
        OrganizationRequest organizationRequest = new OrganizationRequest();
        Organization existingOrganization = new Organization();
        existingOrganization.setId(ORG_ID);
        when(authService.getSignedInUser()).thenReturn(new User());
        when(organizationRepository.findById(ORG_ID)).thenReturn(Optional.of(existingOrganization));

        //Act
        assertDoesNotThrow(() -> organizationService.updateOrganization(ORG_ID, organizationRequest));

        //Assert
        verify(organizationRepository, Mockito.times(1)).save(any());


    }

    @Test
    public void testUpdateOrganization_WhenNotAllowed_ThenThrowNotAllowedException() {
        // Arrange
        OrganizationRequest organizationRequest = new OrganizationRequest();
        Organization existingOrganization = new Organization();
        existingOrganization.setId(2L);
        when(authService.getSignedInUser()).thenReturn(User.builder().build());

        // Act & Assert
        assertThrows(NotAllowedException.class, () -> organizationService.updateOrganization(ORG_ID, organizationRequest));
        verify(organizationRepository, never()).save(any());
    }
}
