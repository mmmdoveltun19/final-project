package com.taskapplication.service;

import com.taskapplication.dto.UserCreateResponse;
import com.taskapplication.dto.UserDTO;
import com.taskapplication.dto.UserRequest;
import com.taskapplication.exception.AlreadyExistsException;
import com.taskapplication.exception.EntityNotFoundException;
import com.taskapplication.exception.NotAllowedException;
import com.taskapplication.mapper.PasswordGeneratorMapper;
import com.taskapplication.mapper.UserMapper;
import com.taskapplication.model.Organization;
import com.taskapplication.model.User;
import com.taskapplication.repository.UserRepository;
import com.taskapplication.service.auth.AuthService;
import com.taskapplication.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userServiceImpl;
    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordGeneratorMapper passwordGeneratorMapper;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private AuthService authService;
    @Mock
    private UserMapper userMapper;

    @Test
    public void testAddUserSuccess() {
        // Arrange
        UserRequest userRequest = new UserRequest("John", "Doe", "john.doe@example.com", "username");
        when(userRepository.existsByEmail(userRequest.getEmail())).thenReturn(false);

        User currentAdmin = new User();
        currentAdmin.setOrganization(new Organization());
        when(authService.getSignedInUser()).thenReturn(currentAdmin);

        when(passwordGeneratorMapper.generatePassword()).thenReturn("generatedPassword");
        when(passwordEncoder.encode("generatedPassword")).thenReturn("encodedPassword");

        // Act
        UserCreateResponse userCreateResponse = userServiceImpl.addUser(userRequest);

        // Assert
        assertEquals("eltun019@example.com", userCreateResponse.getEmail());
        assertEquals("encodedPassword", userCreateResponse.getPassword());
    }

    @Test
    public void testAddUser_AlreadyExists() {
        // Arrange
        UserRequest userRequest = new UserRequest("Eltun", "Mammadov", "eltun.mammadov@example.com", "username");
        when(userRepository.existsByEmail(userRequest.getEmail())).thenReturn(true);

        // Act & Assert
        assertThrows(AlreadyExistsException.class, () -> userServiceImpl.addUser(userRequest));
    }

    @Test
    public void testGetAllUsersSuccess() {
        // Arrange
        User signedInUser = new User();
        signedInUser.setOrganization(new Organization());
        when(authService.getSignedInUser()).thenReturn(signedInUser);

        when(userRepository.findAllByOrganizationId(any())).thenReturn(List.of(new User(), new User()));

        when(userMapper.mapUserToUserDto(any())).thenReturn(new UserDTO());

        // Act
        List<UserDTO> users = userServiceImpl.getAllUsers();

        // Assert
        assertEquals(2, users.size());
    }

    @Test
    public void testGetUserByIdSuccess() {
        // Arrange
        Long userId = 1L;
        User mockUser = new User();
        mockUser.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUser));
        when(userMapper.mapUserToUserDto(mockUser)).thenReturn(new UserDTO());

        // Act
        UserDTO userDTO = userServiceImpl.getUserById(userId);

        // Assert
        assertNotNull(userDTO);
        assertEquals(userId, userDTO.getId());
    }

    @Test
    public void testGetUserByEmailSuccess() {
        // Arrange
        String userEmail = "test@example.com";
        User mockUser = new User();
        mockUser.setEmail(userEmail);

        when(userRepository.findByEmail(userEmail)).thenReturn(Optional.of(mockUser));
        when(userMapper.mapUserToUserDto(mockUser)).thenReturn(new UserDTO());

        // Act
        UserDTO userDTO = userServiceImpl.getUserByEmail(userEmail);

        // Assert
        assertNotNull(userDTO);
        assertEquals(userEmail, userDTO.getEmail());
    }

    @Test
    public void testGetUserByEmailEntityNotFoundException() {
        // Arrange
        String userEmail = "nonexistent@example.com";

        when(userRepository.findByEmail(userEmail)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> userServiceImpl.getUserByEmail(userEmail));
    }

    @Test
    public void testUpdateUserByIdSuccess() {
        // Arrange
        Long userId = 1L;
        User mockUser = new User();
        mockUser.setId(userId);

        UserRequest userRequest = new UserRequest("Eltun", "Mammadov", "eltun.mammadov@example.com", "username");

        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUser));

        // Act
        assertDoesNotThrow(() -> userServiceImpl.updateUserById(userId, userRequest));

        // Assert
        verify(userRepository, times(1)).save(any());
    }

    @Test
    public void testUpdateUserByIdEntityNotFoundException() {
        // Arrange
        Long userId = 2L;
        UserRequest userRequest = new UserRequest("Eltun", "Mammadov", "eltun.mammaov@example.com", "username");

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> userServiceImpl.updateUserById(userId, userRequest));
    }

    @Test
    public void testDeleteUserByIdSuccess() {
        // Arrange
        Long userId = 1L;
        User mockUser = new User();
        mockUser.setId(userId);

        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUser));

        // Act
        assertDoesNotThrow(() -> userServiceImpl.deleteUserById(userId));

        // Assert
        verify(userRepository, times(1)).deleteById(userId);
    }

    @Test
    public void testDeleteUserByIdEntityNotFoundException() {
        // Arrange
        Long userId = 2L;

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> userServiceImpl.deleteUserById(userId));
        verify(userRepository, never()).deleteById(userId);
    }

    @Test
    public void testDeleteUserByIdNotAllowedException() {
        // Arrange
        Long userId = 3L;
        User mockUser = new User();
        mockUser.setId(userId);
        Organization organization = new Organization();
        organization.setId(999L);
        mockUser.setOrganization(organization);

        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUser));
        when(authService.getSignedInUser()).thenReturn(createUserWithOrganizationId(123L));

        // Act & Assert
        assertThrows(NotAllowedException.class, () -> userServiceImpl.deleteUserById(userId));
        verify(userRepository, never()).deleteById(userId);
    }

    private User createUserWithOrganizationId(Long organizationId) {
        User user = new User();
        Organization organization = new Organization();
        organization.setId(organizationId);
        user.setOrganization(organization);
        return user;
    }



}
