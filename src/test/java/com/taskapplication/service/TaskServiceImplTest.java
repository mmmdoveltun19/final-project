package com.taskapplication.service;

import com.taskapplication.dto.TaskDto;
import com.taskapplication.dto.TaskRequest;
import com.taskapplication.enums.Role;
import com.taskapplication.enums.TaskStatus;
import com.taskapplication.exception.EntityNotFoundException;
import com.taskapplication.model.Organization;
import com.taskapplication.model.Task;
import com.taskapplication.model.User;
import com.taskapplication.repository.OrganizationRepository;
import com.taskapplication.repository.TaskRepository;
import com.taskapplication.repository.UserRepository;
import com.taskapplication.service.auth.AuthService;
import com.taskapplication.service.impl.TaskServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTest {

    @InjectMocks
    private TaskServiceImpl taskServiceImpl;

    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private AuthService authService;

    @Test
    void testCreateTask() {
        //Arrange
        TaskRequest taskRequest = new TaskRequest();
        taskRequest.setTitle("Test Task");
        taskRequest.setDescription("Test Description");
        taskRequest.setDeadline(LocalDateTime.now().plusDays(7));
        taskRequest.setAssignId(Collections.singleton(1L));

        Organization organization = new Organization();
        organization.setId(1L);

        User assignedUser = new User();
        assignedUser.setId(1L);
        assignedUser.setOrganization(organization);

        Set<User> assignedUsers = Collections.singleton(assignedUser);

        when(authService.getSignedInUser()).thenReturn(assignedUser);
        when(organizationRepository.findByUsersUserEmail(any())).thenReturn(Optional.of(organization));
        when(userRepository.findById(1L)).thenReturn(Optional.of(assignedUser));

        //Act
        assertDoesNotThrow(() -> taskServiceImpl.createTask(taskRequest));

        //Assert
        verify(taskRepository, times(1)).save(any());
    }

    @Test
    void getTaskById() throws AccessDeniedException {
        //Arrange
        Task task = new Task();
        task.setId(1L);
        task.setTitle("Test Task");
        task.setDescription("Test Description");
        task.setDeadline(LocalDateTime.now().plusDays(7));
        task.setTaskStatus(TaskStatus.PENDING);

        //Act
        when(authService.getSignedInUser()).thenReturn(new User());
        when(taskRepository.findById(1L)).thenReturn(Optional.of(task));
        TaskDto taskDto = taskServiceImpl.getTaskById(1L);

        //Assert
        assertEquals(task.getTitle(), taskDto.getTitle());
        assertEquals(task.getDescription(), taskDto.getDescription());
        assertEquals(task.getTaskStatus(), taskDto.getTaskStatus());
        assertEquals(task.getDeadline(), taskDto.getDeadline());
    }
    @Test
    public void testGetAllTasksByUserId() {
        // Arrange
        Long userId = 1L;
        User user = new User();
        user.setId(userId);

        when(authService.getSignedInUser()).thenReturn(user);

        List<Task> mockedTasks = Arrays.asList(new Task(), new Task(), new Task());
        when(taskRepository.findAllByUsersId(userId)).thenReturn(mockedTasks);

        // Act
        List<TaskDto> result = taskServiceImpl.getAllTasksByUserId(userId);

        // Assert
        assertNotNull(result);
        assertEquals(mockedTasks.size(), result.size());
    }

    @Test
    public void testGetAllTasksByOrganizationId() {
        // Arrange
        Long organizationId = 1L;
        Organization organization = new Organization();
        organization.setId(organizationId);

        when(authService.getSignedInUser()).thenReturn(new User());

        when(organizationRepository.findByUsersUserEmail(anyString())).thenReturn(Optional.of(organization));

        List<Task> mockedTasks = Arrays.asList(new Task(), new Task(), new Task());
        when(taskRepository.findAllByOrganizationId(organizationId)).thenReturn(mockedTasks);

        // Act
        List<TaskDto> result = taskServiceImpl.getAllTasksByOrganizationId(organizationId);

        // Assert
        assertNotNull(result);
        assertEquals(mockedTasks.size(), result.size());
    }

    @Test
    public void testGetAllTasksByStatus() throws AccessDeniedException {
        // Arrange
        Long organizationId = 1L;
        TaskStatus taskStatus = TaskStatus.PENDING;
        Organization organization = new Organization();
        organization.setId(organizationId);
        when(authService.getSignedInUser()).thenReturn(createUser(organization));
        when(taskRepository.findAllByOrganizationId(organizationId)).thenReturn(createTaskList(organization, taskStatus));

        // Act
        List<TaskDto> tasks = taskServiceImpl.getAllTasksByStatus(taskStatus);

        // Assert
        assertEquals(2, tasks.size());
    }


    private User createUser(Organization organization) {
        User user = new User();
        user.setOrganization(organization);
        return user;
    }

    private List<Task> createTaskList(Organization organization, TaskStatus taskStatus) {
        List<Task> tasks = new ArrayList<>();
        Task task1 = new Task();
        task1.setOrganization(organization);
        task1.setTaskStatus(taskStatus);
        tasks.add(task1);

        Task task2 = new Task();
        task2.setOrganization(organization);
        task2.setTaskStatus(taskStatus);
        tasks.add(task2);

        return tasks;
    }

    @Test
    void updateTaskSuccess() throws AccessDeniedException {
        //Arrange
        Long taskId = 1L;
        TaskRequest taskRequest = TaskRequest.builder()
                .title("Test Task")
                .description("Description")
                .deadline(LocalDateTime.now().plusDays(1))
                .status("COMPLETED")
                .build();

        Task existingTask = Task.builder()
                .id(taskId)
                .title("Old Task Title")
                .description("Old Description")
                .deadline(LocalDateTime.now().plusDays(2))
                .taskStatus(TaskStatus.PENDING)
                .organization(new Organization())
                .build();

        when(authService.getSignedInUser()).thenReturn(existingTask.getOrganization().getUsers().iterator().next());

        when(taskRepository.findById(taskId)).thenReturn(java.util.Optional.of(existingTask));

        Task updatedTask = Task.builder()
                .id(taskId)
                .title(taskRequest.getTitle())
                .description(taskRequest.getDescription())
                .deadline(taskRequest.getDeadline())
                .taskStatus(TaskStatus.COMPLETED)
                .organization(existingTask.getOrganization())
                .build();

        //Act
        taskServiceImpl.updateTask(taskId, taskRequest);

        //Assert
        verify(taskRepository, times(1)).save(updatedTask);
    }

    @Test
    void deleteTaskValidTaskIdTaskDeletedSuccessfully() throws AccessDeniedException {
        //Arrange
        Task task = createTask();
        when(authService.getSignedInUser()).thenReturn(createUserWithOrganization());
        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));

        //Act
        taskServiceImpl.deleteTask(taskId);

        //Assert
        verify(taskRepository, times(1)).delete(task);
    }

    @Test
    void deleteTask_InvalidTaskId_EntityNotFoundExceptionThrown() {
        //Arrange
        when(authService.getSignedInUser()).thenReturn(createUserWithOrganization());
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        //Act
        assertThrows(EntityNotFoundException.class, () -> taskServiceImpl.deleteTask(taskId));

        //Assert
        verify(taskRepository, never()).delete(any(Task.class));
    }

    @Test
    void deleteTask_AccessDeniedExceptionThrown() {
        //Arrange
        Task task = createTask();
        when(authService.getSignedInUser()).thenReturn(createUserWithDifferentOrganization());
        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));

        //Act
        assertThrows(AccessDeniedException.class, () -> taskServiceImpl.deleteTask(taskId));

        //Assert
        verify(taskRepository, never()).delete(any(Task.class));
    }

    private Task createTask() {
        Task task = new Task();
        task.setId(task.getId());
        task.setTitle("Sample Task");
        task.setDescription("Sample Task Description");
        task.setDeadline(LocalDateTime.now().plusDays(7));
        task.setTaskStatus(TaskStatus.PENDING);
        task.setUsers(new HashSet<>());
        task.setOrganization(createOrganization());
        return task;
    }

    private User createUserWithOrganization() {
        User user = new User();
        user.setId(1L);
        user.setName("John");
        user.setSurname("Doe");
        user.setEmail("john.doe@example.com");
        user.setPassword("password");
        user.setUsername("john.doe");
        user.setRole(Role.USER);
        user.setOrganization(createOrganization());
        return user;
    }

    private User createUserWithDifferentOrganization() {
        User user = new User();
        user.setId(2L);
        user.setName("Jane");
        user.setSurname("Doe");
        user.setEmail("jane.doe@example.com");
        user.setPassword("password");
        user.setUsername("jane.doe");
        user.setRole(Role.USER);
        user.setOrganization(createOrganization());
        return user;
    }

    private Organization createOrganization() {
        Organization organization = new Organization();
        organization.setId(1L);
        organization.setName("Sample Organization");
        organization.setPhoneNumber("123456789");
        organization.setAddress("Sample Address");
        organization.setUsers(new HashSet<>());
        organization.setTasks(new HashSet<>());
        return organization;
    }
}
