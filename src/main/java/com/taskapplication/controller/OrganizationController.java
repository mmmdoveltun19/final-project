package com.taskapplication.controller;

import com.taskapplication.dto.OrganizationDTO;
import com.taskapplication.dto.OrganizationRequest;
import com.taskapplication.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/organization")
@RequiredArgsConstructor
public class OrganizationController {

    private final OrganizationService organizationService;

    @GetMapping("/{id}")
    public OrganizationDTO getById(@PathVariable Long id) {
        return organizationService.getOrganizationById(id);
    }

    @PutMapping("/{id}")
    public void updateOrganization(@PathVariable Long id,
                                   @RequestBody OrganizationRequest organizationRequest) {
        organizationService.updateOrganization(id, organizationRequest);
    }
}
