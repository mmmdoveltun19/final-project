package com.taskapplication.controller;

import com.taskapplication.dto.ProductDto;
import com.taskapplication.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product/v1")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @PostMapping("product/producer/async")
    public String sendMessageAsync(@RequestBody ProductDto productDto) {
        return productService.sendMessageAsync(productDto);
    }

    @PostMapping("product/producer/sync")
    public String sendMessageSsync(@RequestBody ProductDto productDto) {
        try {
            return productService.sendMessageSync(productDto);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}

