package com.taskapplication.controller;

import com.taskapplication.dto.TaskDto;
import com.taskapplication.dto.TaskRequest;
import com.taskapplication.enums.TaskStatus;
import com.taskapplication.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @PostMapping
    public void createTask(@RequestBody TaskRequest taskRequest) {
        taskService.createTask(taskRequest);
    }

    @PutMapping("/{id}")
    public void updateTaskById(@PathVariable (name = "id") Long id,@RequestBody TaskRequest taskRequest) throws AccessDeniedException {
        taskService.updateTask(id,taskRequest);
    }

    @GetMapping("/{id}")
    public TaskDto getTaskById(@PathVariable (name = "id") Long id) throws AccessDeniedException {
        return taskService.getTaskById(id);
    }

    @GetMapping("/user-tasks/{id}")
    public List<TaskDto> getAllTasksByUserId(@PathVariable(name = "id") Long id){
        return taskService.getAllTasksByUserId(id);
    }

    @GetMapping("/org/{orgId}")
    public List<TaskDto> getAllTaskByOrgId(@PathVariable(name = "orgId") Long id) {
        return taskService.getAllTasksByOrganizationId(id);
    }

    @GetMapping("/status/")
    public List<TaskDto> getTasksByStatusId(@RequestParam(name = "status") String status) {
        return taskService.getAllTasksByStatus(TaskStatus.valueOf(status));
    }

    @DeleteMapping("/{id}")
    public void deleteTaskById(@PathVariable (name = "id") Long id) throws AccessDeniedException{
        taskService.deleteTask(id);
    }
}
