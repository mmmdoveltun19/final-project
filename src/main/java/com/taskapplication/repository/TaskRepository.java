package com.taskapplication.repository;

import com.taskapplication.enums.TaskStatus;
import com.taskapplication.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task,Long> {

    List<Task> findAllByUsersId(Long userId);
    List<Task> findAllByTaskStatus(TaskStatus status);
    List<Task> findAllByOrganizationId(Long organization);
}
