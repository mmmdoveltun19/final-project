package com.taskapplication.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "organizations")
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String phoneNumber;

    @Column(nullable = false)
    String address;

    @OneToMany(mappedBy = "organization", cascade = CascadeType.ALL)
    Set<User> users;

    @OneToMany(mappedBy = "organization", cascade = CascadeType.ALL)
    Set<Task> tasks;
}
