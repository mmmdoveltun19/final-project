package com.taskapplication.exception;

public class EntityNotFoundException extends GenericError{
    public EntityNotFoundException(ErrorCodes code, Object... args) {
        super(code.code, code.code, 400, args);
    }
}
