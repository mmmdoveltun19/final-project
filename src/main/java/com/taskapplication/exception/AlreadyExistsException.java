package com.taskapplication.exception;

public class AlreadyExistsException extends GenericError {

    public AlreadyExistsException(ErrorCodes code, Object... args) {
        super(code.code, code.code, 400, args);
    }

}
