package com.taskapplication.exception;

public class NotAllowedException extends GenericError {
    public NotAllowedException(ErrorCodes code, Object... args) {
        super(code.code, code.code, 400, args);
    }
}
