package com.taskapplication.exception;

public class IncorrectCredentialsException extends GenericError{
    public IncorrectCredentialsException(ErrorCodes code, Object... args) {
        super(code.code, code.code, 400, args);
    }
}
