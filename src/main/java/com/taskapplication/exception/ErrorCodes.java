package com.taskapplication.exception;

import lombok.Getter;


@Getter
public enum ErrorCodes {

//    STUDENT_ID_INVALID("MS19-EXCEPTION-001"),
//    PASSWORD_MISMATCH("MS19-EXCEPTION-002"),
//    USERNAME_ALREADY_REGISTERED("MS19-EXCEPTION-003"),
//    USERNAME_NOT_FOUND("MS19-EXCEPTION-004"),
//    TOKEN_EXPIRED("MS19-EXCEPTION-005")
    ;

    final String code;

    ErrorCodes(String code) {
        this.code = code;
    }
}