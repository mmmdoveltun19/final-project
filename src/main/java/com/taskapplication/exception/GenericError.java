package com.taskapplication.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class GenericError extends RuntimeException {
    public static final long serialVersionUID = 1L;

    protected final int status;
    protected final String code;
    protected final String message;
    protected final transient Object[] arguments;

    public GenericError(String code, String message, int status, Throwable cause, Object... arguments) {
        this(code, message, status, arguments);
        log.info(String.valueOf(cause));
    }

    public GenericError(String code, String message, int status, Object... arguments) {
        super(message);
        this.status = status;
        this.code = code;
        this.message = message;
        this.arguments = arguments == null ? new Object[0] : arguments;
    }

    @Override
    public String toString() {
        return String
                .format("%s{status=%d, code='%s', message='%s'}", this.getClass().getSimpleName(), status,
                        code, message);
    }

}
