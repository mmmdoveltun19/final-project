package com.taskapplication.service;

import com.taskapplication.dto.ProductDto;

public interface ProductService {
    String sendMessageAsync(ProductDto productDto);

    String sendMessageSync(ProductDto productDto) throws Exception;


}

