package com.taskapplication.service.impl;

import com.taskapplication.dto.UserCreateResponse;
import com.taskapplication.dto.UserDTO;
import com.taskapplication.dto.UserRequest;
import com.taskapplication.enums.Role;
import com.taskapplication.exception.AlreadyExistsException;
import com.taskapplication.exception.EntityNotFoundException;
import com.taskapplication.exception.ErrorCodes;
import com.taskapplication.exception.NotAllowedException;
import com.taskapplication.mapper.PasswordGeneratorMapper;
import com.taskapplication.mapper.UserMapper;
import com.taskapplication.model.User;
import com.taskapplication.repository.UserRepository;
import com.taskapplication.service.UserService;
import com.taskapplication.service.auth.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Service
@RequestMapping("/user/v1")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordGeneratorMapper passwordGeneratorMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;
    private final UserMapper userMapper;

    @Override
    public UserCreateResponse addUser(UserRequest userRequest) {
        if (userRepository.existsByEmail(userRequest.getEmail())) {
            throw new AlreadyExistsException(ErrorCodes.valueOf("User with this email already exists"));
        }
        User currentAdmin = authService.getSignedInUser();

        User user = new User();
        user.setEmail(userRequest.getEmail());
        user.setName(userRequest.getName());
        user.setSurname(userRequest.getSurname());
        String password = passwordGeneratorMapper.generatePassword();
        user.setPassword(passwordEncoder.encode(password));
        user.setRole(Role.USER);
        user.setOrganization(currentAdmin.getOrganization());
        user.setUsername(userRequest.getUsername());
        userRepository.save(user);

        return UserCreateResponse.builder()
                .email(user.getEmail())
                .password(password)
                .build();
    }

    @Override
    public List<UserDTO> getAllUsers() {
        User signedInUser = authService.getSignedInUser();
        List<UserDTO> users = userRepository.findAllByOrganizationId(signedInUser
                .getOrganization()
                .getId())
                .stream()
                .map(userMapper::mapUserToUserDto)
                .toList();
        return users;
    }

    @Override
    public UserDTO getUserById(Long id) {
        User user = getUserOrThrow(userRepository.findById(id));
        return userMapper.mapUserToUserDto(user);
    }

    @Override
    public UserDTO getUserByEmail(String email) {
        User user = getUserOrThrow(userRepository.findByEmail(email));
        return userMapper.mapUserToUserDto(user);
    }

    @Override
    public void updateUserById(Long id, UserRequest userRequest) {
        User user = getUserOrThrow(userRepository.findById(id));
        user = User.builder()
                .id(user.getId())
                .email(userRequest.getEmail())
                .name(userRequest.getName())
                .surname(userRequest.getSurname())
                .password(user.getPassword())
                .role(user.getRole())
                .organization(user.getOrganization())
                .username(userRequest.getUsername())
                .tasks(user.getTasks())
                .build();

        userRepository.save(user);

    }

    @Override
    public void deleteUserById(Long id) {
        getUserOrThrow(userRepository.findById(id));
        userRepository.deleteById(id);

    }

    private User getUserOrThrow(Optional<User> optionalUser) {
        Long SignedInUserOrgId = getCurrentOrganizationId();
        User user = optionalUser
                .orElseThrow(() -> new EntityNotFoundException(ErrorCodes.valueOf("User not found")));
        if (!user.getOrganization().getId().equals(SignedInUserOrgId)) {
            throw new NotAllowedException(ErrorCodes.valueOf("You are not allowed to access this user"));
        }
        return user;
    }

    private Long getCurrentOrganizationId() {
        return authService.getSignedInUser()
                .getOrganization()
                .getId();
    }
}