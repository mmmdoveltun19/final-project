package com.taskapplication.service.impl;

import com.taskapplication.dto.ProductDto;
import com.taskapplication.model.Product;
import com.taskapplication.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public String sendMessageAsync(ProductDto productDto) {
        String productId = UUID.randomUUID().toString();

        Product product = Product.builder().
                productId(productId).
                title(productDto.getTitle()).
                price(productDto.getPrice()).
                quantity(productDto.getQuantity()).
                build();

        log.info("***** Before publishing a Product");
        CompletableFuture<SendResult< String, Object>> future =
                kafkaTemplate.send("product-created-events-topic", productId, product);
        future.whenComplete((result, exception) -> {
            if (exception != null) {
                log.info("***** Failed to send message: " + exception.getMessage());
            } else {
                log.info("***** Message sent successfully: " + result.getRecordMetadata());
            }
        });

        log.info("***** Returning product id");
        return productId;

    }

    @Override
    public String sendMessageSync(ProductDto productDto) throws Exception {
        String productId = UUID.randomUUID().toString();

        Product product = Product.builder().
                productId(productId).
                title(productDto.getTitle()).
                price(productDto.getPrice()).
                quantity(productDto.getQuantity()).
                build();

        log.info("***** Before publishing a Product");

        SendResult<String, Object> result =
                kafkaTemplate.send("product-created-events-topic", productId, product).get();

        log.info("***** Topic: " + result.getRecordMetadata().topic());
        log.info("***** Partition: " + result.getRecordMetadata().partition());
        log.info("***** Offset: " + result.getRecordMetadata().offset());

        log.info("***** Returning product id");

        return productId;
    }

}

