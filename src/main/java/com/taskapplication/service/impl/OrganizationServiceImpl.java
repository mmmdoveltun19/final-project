package com.taskapplication.service.impl;

import com.taskapplication.dto.OrganizationDTO;
import com.taskapplication.dto.OrganizationRequest;
import com.taskapplication.exception.EntityNotFoundException;
import com.taskapplication.exception.ErrorCodes;
import com.taskapplication.exception.NotAllowedException;
import com.taskapplication.model.Organization;
import com.taskapplication.repository.OrganizationRepository;
import com.taskapplication.service.OrganizationService;
import com.taskapplication.service.auth.AuthService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
@RequestMapping("/organization/v1")
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final AuthService authService;
    private final ModelMapper modelMapper;

    @Override
    public OrganizationDTO getOrganizationById(Long id) {
        return organizationRepository
                .findById(id)
                .map(org -> modelMapper.map(org, OrganizationDTO.class))
                .orElseThrow(() -> new EntityNotFoundException(ErrorCodes.valueOf("Organization not found")));
    }

    @Override
    public void updateOrganization(Long id, OrganizationRequest organizationRequest) {
        Long loggedInAdmin = authService.getSignedInUser().getOrganization().getId();
        if (!loggedInAdmin.equals(id)) {
            throw new NotAllowedException(ErrorCodes
                    .valueOf("You are not allowed to update this organization"));
        }
        Organization org = organizationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(ErrorCodes
                        .valueOf("Organization not found")));
        org = Organization.builder()
                .id(id)
                .name(organizationRequest.getOrganizationName())
                .address(organizationRequest.getAddress())
                .phoneNumber(organizationRequest.getPhoneNumber())
                .users(org.getUsers())
                .tasks(org.getTasks())

                .build();
        organizationRepository.save(org);
    }

}
