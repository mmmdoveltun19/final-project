package com.taskapplication.service.impl;

import com.taskapplication.dto.TaskDto;
import com.taskapplication.dto.TaskRequest;
import com.taskapplication.enums.TaskStatus;
import com.taskapplication.exception.EntityNotFoundException;
import com.taskapplication.exception.ErrorCodes;
import com.taskapplication.exception.NotAllowedException;
import com.taskapplication.model.Organization;
import com.taskapplication.model.Task;
import com.taskapplication.model.User;
import com.taskapplication.repository.OrganizationRepository;
import com.taskapplication.repository.TaskRepository;
import com.taskapplication.repository.UserRepository;
import com.taskapplication.service.TaskService;
import com.taskapplication.service.auth.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.nio.file.AccessDeniedException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequestMapping("/task/v1")
@RequiredArgsConstructor
@Slf4j
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final AuthService authService;

    @Override
    public void createTask(TaskRequest taskRequest) {
        Organization organization = organizationRepository
                .findByUsersUserEmail(authService.getSignedInUser().getEmail())
                .orElseThrow(() -> new EntityNotFoundException(ErrorCodes.valueOf("Organization not found")));

        Set<User> assignedUsers = new HashSet<>();
        for (Long userId : taskRequest.getAssignId()) {
            User user = userRepository.findById(userId)
                    .orElseThrow(() -> new EntityNotFoundException(ErrorCodes.valueOf("User not found")));

            if (!user.getOrganization().equals(organization)) {
                throw new NotAllowedException(ErrorCodes.valueOf("You are not allowed to assign this user"));
            }
            assignedUsers.add(user);
        }

        Task task = Task.builder()
                .title(taskRequest.getTitle())
                .description(taskRequest.getDescription())
                .deadline(taskRequest.getDeadline())
                .taskStatus(TaskStatus.PENDING)
                .users(assignedUsers)
                .organization(organization)
                .build();

        taskRepository.save(task);
        log.info("Task created successfully");

    }

    @Override
    public TaskDto getTaskById(Long taskId) throws AccessDeniedException {
        return mapTaskToDto(getByTaskId(taskId));
    }

    @Override
    public List<TaskDto> getAllTasksByOrganizationId(Long organizationId) {
        checkIfOrgMatches(organizationId);
        List<TaskDto> tasks = taskRepository.findAllByOrganizationId(organizationId)
                .stream()
                .map(this::mapTaskToDto)
                .toList();
        return tasks;
    }

    @Override
    public List<TaskDto> getAllTasksByUserId(Long userId) {
        List<TaskDto> tasks = taskRepository.findAllByUsersId(userId)
                .stream()
                .map(this::mapTaskToDto)
                .collect(Collectors.toList());
        return tasks;
    }

    @Override
    public List<TaskDto> getAllTasksByStatus(TaskStatus taskStatus) {
        List<TaskDto> tasks = taskRepository.findAllByOrganizationId(authService.getSignedInUser()
                        .getOrganization()
                        .getId())
                .stream()
                .filter(task -> task.getTaskStatus().equals(taskStatus))
                .map(this::mapTaskToDto)
                .collect(Collectors.toList());
        return tasks;
    }

    @Override
    public void updateTask(Long taskId, TaskRequest taskRequest) throws AccessDeniedException {
        Task task = getByTaskId(taskId);
        task = Task.builder()
                .id(taskId)
                .title(taskRequest.getTitle())
                .description(taskRequest.getDescription())
                .deadline(taskRequest.getDeadline())
                .taskStatus(TaskStatus.valueOf(taskRequest.getStatus().toUpperCase()))
                .organization(task.getOrganization())
                .build();
        taskRepository.save(task);
    }

    @Override
    public void deleteTask(Long taskId) throws AccessDeniedException {
        taskRepository.delete(getByTaskId(taskId));
    }

    private Task getByTaskId(Long taskId) throws NotAllowedException {
        Organization signedInUserOrganization = authService.getSignedInUser().getOrganization();
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException(ErrorCodes.valueOf("Task was not found")));

        if (!task.getOrganization().equals(signedInUserOrganization)) {
            throw new NotAllowedException(ErrorCodes.valueOf("You are not allowed to access this task"));
        } else
            return task;

    }

    private void checkIfOrgMatches(Long organizationId) {
        Long signedInUserOrgId = authService.getSignedInUser().getOrganization().getId();
        if (!Objects.equals(signedInUserOrgId, organizationId)) {
            throw new NotAllowedException(ErrorCodes.valueOf("You are not allowed to access this organization"));
        }
    }

    private TaskDto mapTaskToDto(Task task) {
        return TaskDto.builder()
                .title(task.getTitle())
                .description(task.getDescription())
                .taskStatus(task.getTaskStatus())
                .deadline(task.getDeadline())
                .build();
    }
}