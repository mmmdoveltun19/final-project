package com.taskapplication.service;

import com.taskapplication.dto.OrganizationDTO;
import com.taskapplication.dto.OrganizationRequest;

public interface OrganizationService {
    OrganizationDTO getOrganizationById(Long id);

    void updateOrganization(Long id, OrganizationRequest organizationRequest);

}
