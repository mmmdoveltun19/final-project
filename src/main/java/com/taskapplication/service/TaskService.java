package com.taskapplication.service;

import com.taskapplication.dto.TaskDto;
import com.taskapplication.dto.TaskRequest;
import com.taskapplication.enums.TaskStatus;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface TaskService {
    void createTask(TaskRequest taskRequest);
    TaskDto getTaskById(Long taskId) throws AccessDeniedException;
    List<TaskDto> getAllTasksByOrganizationId(Long organizationId);
    List<TaskDto> getAllTasksByUserId(Long userId);
    List<TaskDto> getAllTasksByStatus(TaskStatus taskStatus);
    void updateTask(Long taskId, TaskRequest taskRequest) throws AccessDeniedException;
    void deleteTask(Long taskId) throws AccessDeniedException;

}
