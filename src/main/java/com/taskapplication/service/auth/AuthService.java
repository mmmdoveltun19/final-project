package com.taskapplication.service.auth;

import com.taskapplication.dto.authDto.AuthResponse;
import com.taskapplication.dto.authDto.SignInRequest;
import com.taskapplication.dto.authDto.SignUpRequest;
import com.taskapplication.enums.Role;
import com.taskapplication.exception.AlreadyExistsException;
import com.taskapplication.exception.EntityNotFoundException;
import com.taskapplication.exception.ErrorCodes;
import com.taskapplication.exception.IncorrectCredentialsException;
import com.taskapplication.model.Organization;
import com.taskapplication.model.User;
import com.taskapplication.repository.OrganizationRepository;
import com.taskapplication.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authonticatioManager;
    private final OrganizationRepository organizationRepository;
    private final UserDetailsServiceImpl userDetailsServiceImpl;

    public AuthResponse signIn(SignInRequest signInRequest) {
        try {
            authonticatioManager.authenticate(
                    new UsernamePasswordAuthenticationToken(signInRequest.getEmail(), signInRequest.getPassword())
            );
            UserDetails user = userDetailsServiceImpl.loadUserByUsername(signInRequest.getEmail());
            String generatedToken = jwtService.generateToken(user);
            log.info("User signed in successfully with email: {}", user.getUsername());
            return AuthResponse.builder()
                    .token(generatedToken)
                    .message("Successfully signed in")
                    .build();

        } catch (AuthenticationException e) {
            throw new IncorrectCredentialsException(ErrorCodes.valueOf("Incorrect username or password"));
        }
    }

    @Transactional
    public AuthResponse signUp(SignUpRequest signUpRequest) {
        List<String> errors = new ArrayList<>();

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            errors.add("User with this username already exists");
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            errors.add("User with this email already exists");
        }
        if (organizationRepository.existsByName(signUpRequest.getOrganizationName())) {
            errors.add("Organization with this name already exists");
        }

        if (!errors.isEmpty()) {
            String errorMessage = String.join(", ", errors);
            throw new AlreadyExistsException(ErrorCodes.valueOf(errorMessage));
        }

        Organization organization = Organization.builder()
                .name(signUpRequest.getOrganizationName())
                .address(signUpRequest.getOrganizationAddress())
                .phoneNumber(signUpRequest.getOrganizationPhoneNumber())
                .build();
        organizationRepository.save(organization);

        var user = User.builder()
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .organization(organization)
                .role(Role.ADMIN)
                .build();
        userRepository.save(user);

        UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(user.getEmail());
        String generatedToken = jwtService.generateToken(userDetails);
        log.info("User registered successfully with email: {}", user.getEmail());


        return AuthResponse.builder()
                .token(generatedToken)
                .build();
    }

    public User getSignedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User signedInUser = userRepository
                .findByEmail(authentication.getName())
                .orElseThrow(() -> new EntityNotFoundException(ErrorCodes.valueOf("User not found")));
        return signedInUser;
    }
}

