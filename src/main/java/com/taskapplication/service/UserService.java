package com.taskapplication.service;

import com.taskapplication.dto.UserCreateResponse;
import com.taskapplication.dto.UserDTO;
import com.taskapplication.dto.UserRequest;

import java.util.List;

public interface UserService {
    UserCreateResponse addUser(UserRequest userRequest);

    List<UserDTO> getAllUsers();

    UserDTO getUserById(Long id);

    UserDTO getUserByEmail(String email);

    void updateUserById(Long id, UserRequest userRequest);

    void deleteUserById(Long id);


}
