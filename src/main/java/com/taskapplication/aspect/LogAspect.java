package com.taskapplication.aspect;

import com.taskapplication.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class LogAspect {

//    private final CacheManager cacheManager;

    @Before(value = "pointcutGenerateMethod()" +
            "&& args(userId,..)", argNames = "joinPoint,userId")
    public void beforeGenerateMethod(JoinPoint joinPoint, Long userId) {
        log.info("-Before advice: UserController-> " + joinPoint.getSignature().getName());
    }

    @After("pointcutGenerateMethod() && args(userID)")
    public void afterGenerateMethod(JoinPoint joinPoint, Long userID){
        log.info("-After advice: UserController-> " + joinPoint.getSignature().getName());
    }

//    @Around("pointcutGenerateMethod()" +
//            "&& args(id,..)")
//    public Object getFromCache(ProceedingJoinPoint joinPoint, Long id) throws Throwable {
//        Cache cache = cacheManager.getCache("user");
//        User user = cache.get(id, User.class);
//        if (user == null) {
//            user = (User) joinPoint.proceed();
//            cache.put(id, user);
//        }
//        return user;
//    }

    @AfterReturning("pointcutGenerateMethod() && args(id)")
    public void afterReturningGenerateMethod(JoinPoint joinPoint, Integer id) {
        log.info("-AfterReturning advice: UserController-> " + joinPoint.getSignature().getName());
    }

    @AfterThrowing(value = "pointcutGenerateMethod()", throwing = "exception")
    public void logAfterThrowing(JoinPoint joinPoint, Exception exception) {
        log.info("-AfterThrowing advice: -> " + joinPoint.getSignature().getName());
        log.info("Exception is:" + exception.getMessage());
    }

    @Around("@annotation(UserRole)")
    public Object userRole(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - start;
        log.info(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }

    @Pointcut("execution(* com.taskapplication.controller.UserController.*(..))")
    private void pointcutGenerateMethod() {

    }
}