package com.taskapplication.aspect;

import com.taskapplication.enums.Role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation
 * Allow proceeding only with given authentication method
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UserRole {

    /**
     * value
     * Authentication type
     */
    Role value();
}