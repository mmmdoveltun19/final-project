package com.taskapplication.mapper;

import com.taskapplication.dto.UserDTO;
import com.taskapplication.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserMapper {

    private final TaskMapper taskMapper;

    public UserDTO mapUserToUserDto(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .surname(user.getSurname())
                .username(user.getUsername())
                .tasks(user.getTasks()
                        .stream()
                        .map(taskMapper::mapTaskToDto)
                        .collect(Collectors.toSet()))
                .build();
    }
}
