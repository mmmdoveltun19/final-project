package com.taskapplication.mapper;

import com.taskapplication.dto.TaskDto;
import com.taskapplication.model.Task;
import org.springframework.stereotype.Component;

@Component
public class TaskMapper {

    public TaskDto mapTaskToDto(Task task) {
        return TaskDto.builder()
                .title(task.getTitle())
                .description(task.getDescription())
                .deadline(task.getDeadline())
                .taskStatus(task.getTaskStatus())
                .build();
    }
}
