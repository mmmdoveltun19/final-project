package com.taskapplication.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRequest {

    @NotBlank(message = "Name cannot be empty")
    String name;

    @NotBlank(message = "Surname cannot be empty")
    String surname;

    @NotBlank(message = "Username cannot be empty")
    String username;

    @Email(message = "Email should be valid")
    String email;
}
