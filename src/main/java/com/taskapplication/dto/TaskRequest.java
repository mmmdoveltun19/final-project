package com.taskapplication.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskRequest {

    @NotBlank
    String title;
    @NotBlank
    String description;

    @Enumerated(EnumType.STRING)
    @NotBlank
    String status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "UTC")
    LocalDateTime deadline;

    List<Long> assignId;
}
