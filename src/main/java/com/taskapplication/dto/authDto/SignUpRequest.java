package com.taskapplication.dto.authDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignUpRequest {

    private String username;
    private String email;
    private String password;
    private String organizationName;
    private String organizationAddress;
    private String organizationPhoneNumber;
}
