package com.taskapplication.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.taskapplication.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskDto {

    String title;

    String description;

    LocalDateTime deadline;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "UTC")
    TaskStatus taskStatus;
}