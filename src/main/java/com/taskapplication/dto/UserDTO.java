package com.taskapplication.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO {

    Long id;

    String name;

    String surname;

    String email;

    String password;

    String username;

    Set<TaskDto> tasks = new HashSet<>();
}
